var billingId = context.getVariable("request.queryparam.billingId");
var last = context.getVariable("request.queryparam.last");

var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

var regex = new RegExp("^[0-9]{4,}$");
var regexLast = new RegExp("^(true|false|TRUE|FALSE|True|False)$");

if (billingId !== null) {
    var billingIds = billingId.split(",");
    (function validateBillingId(){
        for(var i=0;i<billingIds.length;i++){
            if(!regex.test(billingIds[i])){
                context.setVariable("isRequestValid","false");
                    context.setVariable("queryParam", "billingId");
                    context.setVariable("errorMessage", "One of the billingId is invalid, Invalid billingId " 
                    + billingIds[i]);
                
                return "false";
            }
        }
        
        }()
    )
}else {
    context.setVariable("isRequestValid","false");
    context.setVariable("queryParam", "billingId");
    context.setVariable("errorMessage", "Please provide a valid billingId");
}
    
if(last !== null && !regexLast.test(last)) {
    context.setVariable("isRequestValid", "false");
    context.setVariable("queryParam", "last");
    context.setVariable("errorMessage", "Invalid last is provided.");
} else if (regexLast.test(last)) {
    context.setVariable("request.queryparam.last", last.toLowerCase());
}